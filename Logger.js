const
    _chalk = require("chalk"),
    util = require("util"),
    _ = require("lodash");
const chalk = new _chalk.constructor({enabled: true}),
    validChalkStyles = ['reset', 'bold', 'dim', 'italic', 'underline', 'inverse', 'hidden', 'strikethrough', 'black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white', 'gray', 'grey', 'bgBlack', 'bgRed', 'bgGreen', 'bgYellow', 'bgBlue', 'bgMagenta', 'bgCyan', 'bgWhite'],
    eol = require("os").EOL;


class Logger {
    constructor(options) {
        if (util.isString(options)) options = {header: options};
        this.options = Object.assign({}, {
            timestamp: true,
            color: true,
            header: "",
            headerPadding: {
                amount: 8,
                direction: "left"
            },
            eol: require("os").EOL,
            levels: [
                new Level({function_call: "info"}),
                new Level({function_call: "log", header: "[INFO]"}),
                new Level({function_call: "debug", colors: {header: ["cyan"], text: ["cyan"]}}),
                new Level({
                    function_call: "fatal",
                    colors: {header: ["bgWhite", "red"], text: ["bgWhite", "red"]},
                    stream: process.stderr
                }),
                new Level({
                    function_call: "critical",
                    colors: {header: ["bgWhite", "red"], text: ["bgWhite", "red"]},
                    stream: process.stderr
                }),
                new Level({
                    function_call: "error",
                    colors: {header: ["red"], text: ["red"]},
                    stream: process.stderr
                }),
                new Level({function_call: "trace", colors: {header: ["gray"], text: ["gray"]}}),
                new Level({function_call: "warn", colors: {header: ["yellow"], text: ["yellow"]}})
            ]
        }, options);

        this.defaultErrorLevel = new Level({
            colors: {
                header: ["bgWhite", "red"],
                text: ["reset", "red"]
            },
            function_call: "error"
        });
        this.defaultInfoLevel = new Level({
            function_call: "info"
        });

        _.each(this.options.levels, function(level) {
            this.addLevel(level);
        }.bind(this));
    }

    static _write(msg, stream) {
        if (util.isNullOrUndefined(stream) || !util.isFunction(stream.write)) stream = process.stdout;
        stream.write(msg);
    }

    _format(level, message) {
        if (!(level instanceof Level)) level = this.options.levels[0];
        let headerChalk = level.headerChalk;
        let textChalk = level.textChalk;

        let pieces = {
            time: this.time,
            header: this.header.length > 0 ? `(${this.header}) ` : "",
            levelSpaces: getPadSpaces(level.header, util.isNumber(this.options.headerPadding.amount) ? this.options.headerPadding.amount : 7),
            level: level.header,
            text: message,
            eol: require("os").EOL
        };

        return `${pieces.time} ${headerChalk(`${pieces.header}`)}${this.options.headerPadding.direction.toLowerCase().trim() == "left" ? headerChalk(pieces.levelSpaces + pieces.level) : headerChalk(pieces.level + pieces.levelSpaces)}${textChalk(": " + pieces.text)}${pieces.eol}`;
    }

    addLevel(level) {
        if (!(level instanceof Level)) {
            try {
                level = new Level(level);
            } catch (e) {
                Logger._write(this._format(this.defaultErrorLevel, `Failed to parse the level: ${util.inspect(level, inspectOpts)}`));
                return;
            }
        }
        let inspectOpts = {colors: this.options.color === true, depth: 5};
        this[level.function_call] = function() {
            let toPrint = [];
            _.each(arguments, arg => {
                if (arg instanceof Error) {
                    arg = `${eol}${arg.stack || arg.toString()}${eol}`;
                } else if (!util.isString(arg)) {
                    arg = util.inspect(arg, inspectOpts);
                }
                toPrint.push(arg);
            });
            Logger._write(this._format(level, toPrint.join(" ")), level.stream);
        }.bind(this);
    }

    set header(val) {
        this.options.header = val || "";
    }

    get header() {
        return this.options.header || "";
    }

    get time() {
        return this.options.timestamp !== false ? `[${new Date().toLocaleString()}]` : ``;
    }
}

class Level {
    constructor(options) {
        let opts = Object.assign({}, {
            colors: {
                header: [],
                text: []
            },
            header: null,
            function_call: null,
            stream: null
        }, options);

        if (util.isString(opts.bgcolor)) { //backwards compatibility for new Level({...bgcolor: "red"...});
            opts.colors.header.push(opts.bgcolor);

            if (opts.color_everything === true) {
                opts.colors.text.push(opts.bgcolor);
            }

            delete opts.bgcolor;
        }
        if (util.isString(opts.color)) { //backwards compatibility for new Level({...color: "red"...});
            opts.colors.header.push(opts.color);

            if (opts.color_everything === true) {
                opts.colors.text.push(opts.color);
            }

            delete opts.color;
        }

        this.options = opts;

        if (util.isNullOrUndefined(opts.function_call) || !util.isString(opts.function_call)) throw new TypeError("Missing the 'function_call' option for this level.");
        if (opts.function_call.includes(" ")) opts.function_call.replace(/ /g, "_");
        if (util.isNullOrUndefined(opts.header)) opts.header = `[${opts.function_call.toUpperCase()}]`;
    }

    set stream(val) {
        if (!(val instanceof (require("stream")))) {
            process.stdout.write(`[WARN] level.stream expected a Stream object. Got: ${typeof val}. Aborting stream set\n`);
            return;
        }
        this.options.stream = val;
    }

    get stream() {
        return this.options.stream;
    }

    get color_everything() {
        return this.options.color_everything;
    }

    get colors() {
        return this.options.colors;
    }

    get header() {
        return this.options.header;
    }

    get function_call() {
        return this.options.function_call;
    }

    get headerChalk() {
        return Level.parseChalkArray(this.colors.header);
    }

    get textChalk() {
        return Level.parseChalkArray(this.colors.text);
    }

    static parseChalkArray() {
        let loopThrough = arguments,
            toRet = chalk.reset;
        if (util.isArray(arguments[0])) loopThrough = arguments[0];
        // console.info("Looping through", loopThrough);

        for (let i = 0; i < loopThrough.length; i++) {
            if (!util.isNullOrUndefined(chalk[loopThrough[i]])) {
                // console.info("injecting ", loopThrough[i]);
                toRet = toRet[loopThrough[i]];
            }
        }

        return toRet;
    }
}

function padRight(string, targetLength, padString) {
    targetLength = targetLength >> 0; //floor if number or convert non-number to 0;
    padString = String(padString || ' ');
    if (string.length > targetLength) {
        return String(string);
    } else {
        targetLength = targetLength - string.length;
        if (targetLength > padString.length) {
            padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
        }
        return String(string) + padString.slice(0, targetLength);
    }
}
function padLeft(string, targetLength, padString) {
    targetLength = targetLength >> 0;
    padString = String(padString || ' ');
    if (string.length > targetLength) {
        return String(string);
    } else {
        targetLength = targetLength - string.length;
        if (targetLength > padString.length) {
            padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
        }
        return padString.slice(0, targetLength) + String(string);
    }
}

function getPadSpaces(string, targetLength) {
    targetLength = targetLength >> 0;
    if (string.length < targetLength) targetLength = targetLength - string.length; else return "";
    return " ".repeat(targetLength);
}

module.exports = Logger; //new (require("Logger"));
module.exports.Level = Level; //new (require("Logger").Level);
module.exports.Logger = Logger; //new (require("Logger").Logger)